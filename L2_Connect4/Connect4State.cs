﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace L2_Connect4
{
    class Connect4State : State
    {

        private const int POINTS_3 = 16;
        private const int POINTS_2 = 4;
        private const int POINTS_1 = 1;

        private string id;
        private int[,] grid;
        private int width;
        private int height;
        private Player currentPlayer;

        public override string ID => id;
        public int[,] Grid { get => grid; set => grid = value; }
        public int Width { get => width; set => width = value; }
        public int Height { get => height; set => height = value; }
        public Player CurrentPlayer => currentPlayer;

        public enum Player
        {
            MIN = 1,
            MAX = 2
        }

        public Connect4State(int height, int width, Player startingPlayer) : base()
        {
            depth = 0;
            currentPlayer = (startingPlayer == Player.MAX) ? Player.MIN : Player.MAX;
            this.height = height;
            this.width = width;

            grid = new int[height, width];

            for (int i = 0; i < height; i++)
            {
                for (int j = 0; j < width; j++)
                {
                    grid[i, j] = 0;
                }
            }

            StringBuilder builder = new StringBuilder();
            foreach (var item in grid)
            {
                builder.Append(item);
            }
            id = builder.ToString();

            h = 0;
        }

        public Connect4State(Connect4State parent, int moveCol, bool playerMove) : base(parent)
        {
            height = parent.height;
            width = parent.width;

            if (moveCol >= width)
            {
                throw new ArgumentException("Column outside of grid");
            }

            currentPlayer = (parent.currentPlayer == Player.MAX) ? Player.MIN : Player.MAX;

            grid = new int[height, width];
            Array.Copy(parent.grid, grid, grid.Length);

            if (grid[0, moveCol] != 0)
            {
                throw new ArgumentException("Illegal move");
            }

            for (int i = height - 1; i >= 0; i--)
            {
                if (grid[i, moveCol] == 0)
                {
                    grid[i, moveCol] = (int)currentPlayer;
                    break;
                }
            }

            StringBuilder builder = new StringBuilder();
            foreach (var item in grid)
            {
                builder.Append(item);
            }
            id = builder.ToString();

            if (playerMove)
            {
                depth = 0;
                children = new List<IState>();
                Parent = null;
                rootMove = null;
            }
            else
            {
                depth = parent.depth + 0.5;

                if (parent.rootMove == null)
                {
                    rootMove = id;
                }
                else
                {
                    rootMove = parent.rootMove;
                }
            }

            h = ComputeHeuristicGrade();
        }

        public Connect4State(Connect4State parent, string id) : base()
        {
            depth = 0;
            height = parent.height;
            width = parent.width;
            currentPlayer = (parent.currentPlayer == Player.MAX) ? Player.MIN : Player.MAX;

            grid = new int[height, width];
            this.id = id;

            if (id == null)
            {
                Array.Copy(parent.grid, grid, grid.Length);

                int moveCol = 0;
                for (int j = 0; j < width; j++)
                {
                    if (grid[0, j] == 0)
                    {
                        moveCol = j;
                        break;
                    }
                }

                for (int i = 0; i < height; i++)
                {
                    if (grid[i, moveCol] == 0)
                    {
                        grid[i, moveCol] = (int)currentPlayer;
                        break;
                    }
                }
            }
            else
            {
                for (int i = 0; i < height; i++)
                {
                    for (int j = 0; j < width; j++)
                    {
                        grid[i, j] = id[i * (height + 1) + j] - '0';
                    }
                }
            }

            h = ComputeHeuristicGrade();
        }

        public override double ComputeHeuristicGrade()
        {
            double score = 0;

            for (Player player = Player.MIN; player <= Player.MAX; player++)
            {
                int playerMod = (player == Player.MAX) ? 1 : -1;
                double result = 0;

                result = CalcScoreInRow(player);
                if (double.IsInfinity(result))
                {
                    return result;
                }
                else
                {
                    score += result * playerMod;
                }

                result = CalcScoreInCol(player);
                if (double.IsInfinity(result))
                {
                    return result;
                }
                else
                {
                    score += result * playerMod;
                }

                result = CalcScoreLeftDiag(player);
                if (double.IsInfinity(result))
                {
                    return result;
                }
                else
                {
                    score += result * playerMod;
                }

                result = CalcScoreRightDiag(player);
                if (double.IsInfinity(result))
                {
                    return result;
                }
                else
                {
                    score += result * playerMod;
                }

                //score += CalcScoreSingle(player) * playerMod;
            }

            return score;
        }

        private double CalcScoreInRow(Player player)
        {
            double score = 0;

            for (int i = 0; i < height; i++)
            {
                int inRows = 0;
                for (int j = 0; j < width - 1; j++)
                {
                    if (grid[i, j] == (int)player && grid[i, j] == grid[i, j + 1])
                    {
                        inRows++;
                    }
                }
                if (inRows == 3)
                {
                    if (player == Player.MAX)
                    {
                        return double.PositiveInfinity;
                    }
                    else
                    {
                        return double.NegativeInfinity;
                    }
                }
                else if (inRows == 2)
                {
                    score += POINTS_3;
                }
                else if (inRows == 1)
                {
                    score += POINTS_2;
                }
            }

            return score;
        }

        private double CalcScoreInCol(Player player)
        {
            double score = 0;

            for (int i = 0; i < width; i++)
            {
                int inCols = 0;
                for (int j = 0; j < height - 1; j++)
                {
                    if (grid[j, i] == (int)player && grid[j, i] == grid[j + 1, i])
                    {
                        inCols++;
                    }
                }
                if (inCols == 3)
                {
                    if (player == Player.MAX)
                    {
                        return double.PositiveInfinity;
                    }
                    else
                    {
                        return double.NegativeInfinity;
                    }
                }
                else if (inCols == 2)
                {
                    score += POINTS_3;
                }
                else if (inCols == 1)
                {
                    score += POINTS_2;
                }
            }

            return score;
        }

        private double CalcScoreRightDiag(Player player)
        {
            double score = 0;

            for (int i = 0; i < height - 1; i++)
            {
                for (int j = 0; j < width - 1; j++)
                {
                    int inDiag = 0;
                    if (grid[i, j] == (int)player)
                    {
                        for (int row = i, col = j; (row < height - 1) && (col < width - 1); row++, col++)
                        {
                            if (grid[row, col] == (int)player && grid[row, col] == grid[row + 1, col + 1])
                            {
                                inDiag++;
                            }
                        }
                    }
                    if (inDiag == 3)
                    {
                        if (player == Player.MAX)
                        {
                            return double.PositiveInfinity;
                        }
                        else
                        {
                            return double.NegativeInfinity;
                        }
                    }
                    else if (inDiag == 2)
                    {
                        score += POINTS_3;
                    }
                    else if (inDiag == 1)
                    {
                        score += POINTS_2;
                    }
                }
            }

            return score;
        }

        private double CalcScoreLeftDiag(Player player)
        {
            double score = 0;

            for (int i = 0; i < height - 1; i++)
            {
                for (int j = 1; j < width; j++)
                {
                    int inDiag = 0;
                    if (grid[i, j] == (int)player)
                    {
                        for (int row = i, col = j; (row < height - 1) && (col > 0); row++, col--)
                        {
                            if (grid[row, col] == (int)player && grid[row, col] == grid[row + 1, col - 1])
                            {
                                inDiag++;
                            }
                        }
                    }
                    if (inDiag == 3)
                    {
                        if (player == Player.MAX)
                        {
                            return double.PositiveInfinity;
                        }
                        else
                        {
                            return double.NegativeInfinity;
                        }
                    }
                    else if (inDiag == 2)
                    {
                        score += POINTS_3;
                    }
                    else if (inDiag == 1)
                    {
                        score += POINTS_2;
                    }
                }
            }

            return score;
        }

        private double CalcScoreSingle(Player player)
        {
            double score = 0;

            for (int i = 0; i < height; i++)
            {
                for (int j = 0; j < width; j++)
                {
                    if (grid[i, j] == (int)player)
                    {
                        score += POINTS_1;
                    }
                }
            }

            return score;
        }

        public bool IsDraw()
        {
            foreach (var item in grid)
            {
                if (item == 0)
                {
                    return false;
                }
            }

            return true;
        }

        public void Print()
        {
            for (int i = 0; i < height; i++)
            {
                for (int j = 0; j < width; j++)
                {
                    if (grid[i, j] == 0)
                    {
                        Console.Write("  ");
                    }
                    else if (grid[i, j] == (int)Player.MAX)
                    {
                        Console.BackgroundColor = ConsoleColor.Red;
                        Console.Write("x ");
                        Console.ResetColor();
                    }
                    else
                    {
                        Console.BackgroundColor = ConsoleColor.Blue;
                        Console.Write("o ");
                        Console.ResetColor();
                    }
                }
                Console.WriteLine();
            }
            Console.WriteLine();
        }
    }
}
