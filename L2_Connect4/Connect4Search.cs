﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace L2_Connect4
{
    class Connect4Search : AlphaBetaSearcher
    {
        public Connect4Search(IState startState, bool isMaximizingPlayerFirst, double maximumDepth)
            : base(startState, isMaximizingPlayerFirst, maximumDepth)
        {
        }

        protected override void BuildChildren(IState parent)
        {
            Connect4State state = parent as Connect4State;

            for (int j = 0; j < state.Width; j++)
            {
                if (state.Grid[0, j] == 0)
                {
                    Connect4State child = new Connect4State(state, j, false);
                    parent.Children.Add(child);
                }
            }
        }
    }
}

