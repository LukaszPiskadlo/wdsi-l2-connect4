﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace L2_Connect4
{
    class Program
    {
        static void Main(string[] args)
        {
            Program game = new Program();
            game.Play();

            Console.WriteLine("\n\nPress any key to quit");
            Console.ReadKey();
        }

        private void Play()
        {
            Console.Write("Input board height [5]: ");
            int height = GetUserInput(5);

            Console.Write("Input board width [6]: ");
            int width = GetUserInput(6);

            Console.Write("Input max depth [3]: ");
            int maxDepth = GetUserInput(3);

            bool isBotMax = false;

            Connect4State state = new Connect4State(height, width, Connect4State.Player.MAX);

            while (true)
            {
                for (int i = 1; i <= width; i++)
                {
                    Console.Write(i + " ");
                }
                Console.WriteLine();

                Console.Write("Podaj numer kolumny: ");
                int col = Int32.Parse(Console.ReadLine());

                state = new Connect4State(state, col - 1, true);

                if (double.IsNegativeInfinity(state.H))
                {
                    state.Print();
                    Console.WriteLine("Min player won!");
                    break;
                }
                else if (double.IsPositiveInfinity(state.H))
                {
                    state.Print();
                    Console.WriteLine("Max player won!");
                    break;
                }
                else if (state.IsDraw())
                {
                    state.Print();
                    Console.WriteLine("Draw!");
                    break;
                }

                Connect4Search searcher = new Connect4Search(state, isBotMax, maxDepth);
                searcher.DoSearch();

                double cmp = 0;
                if (isBotMax)
                {
                    cmp = double.NegativeInfinity;
                }
                else
                {
                    cmp = double.PositiveInfinity;
                }

                KeyValuePair<string, double> result = new KeyValuePair<string, double>();
                foreach (var item in searcher.MovesMiniMaxes)
                {

                    if (!isBotMax && item.Value < cmp)
                    {
                        cmp = item.Value;
                        result = item;
                    }

                    if (isBotMax && item.Value > cmp)
                    {
                        cmp = item.Value;
                        result = item;
                    }
                }

                state = new Connect4State(state, result.Key);

                Console.WriteLine("H = " + state.H);
                state.Print();

                if (double.IsNegativeInfinity(state.H))
                {
                    Console.WriteLine("Min player won!");
                    break;
                }
                else if (double.IsPositiveInfinity(state.H))
                {
                    Console.WriteLine("Max player won!");
                    break;
                }
                else if (state.IsDraw())
                {
                    Console.WriteLine("Draw!");
                    break;
                }
            }
        }

        private int GetUserInput(int defValue)
        {
            string input = Console.ReadLine().Trim();
            int value;
            if (String.IsNullOrEmpty(input))
                value = defValue;
            else
                value = int.Parse(input);

            return value;
        }
    }
}
